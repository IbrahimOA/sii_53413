#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "DatosMemCompartida.h"

using namespace std; 

int main(void)
{
      
	char * p_org;
	DatosMemCompartida * p_mem;
	char *mem="./tmp/mem.txt";

        
        //Abre fichero origen o entrada 
        int fd_mem = open(mem, O_RDWR);
        if (fd_mem < 0) {
           perror("Error apertura fichero origen");
           exit(1);
        }
	if ((p_org=(char *)mmap(NULL, sizeof (p_mem), PROT_WRITE | PROT_READ, MAP_SHARED, fd_mem, 0))==MAP_FAILED){
		perror("Error en la proyeccion del fichero origen");
		close(fd_mem);
		exit(1);
	}
	
	// Se cierran los ficheros 
	close(fd_mem); 

	//asignar la direccio de comienzo de la region creada 
	
	p_mem=(DatosMemCompartida *)p_org;

	int respuesta;
	float centro_raqueta;
	while(1)
	{
 		centro_raqueta=(p_mem->raqueta1.y1+p_mem->raqueta1.y2)/2;
		if(centro_raqueta<p_mem->esfera.centro.y)
			p_mem->accion=1;
		else if (centro_raqueta>p_mem->esfera.centro.y)
			p_mem->accion=-1;
		else {p_mem->accion=0; usleep (25000);}
			
	}		

	
}
