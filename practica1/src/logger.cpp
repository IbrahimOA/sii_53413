//utilizar $ strace ./<nombre_programa>
#pragma once
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <iostream>
#define MAX 1024


int main() {

	
	int fd=0;
	char cadena[MAX];
	char *tuberia= "/tmp/FIFO";
        
	//Creamos un archivo FIFO de nombre FIFO y permisos 0666
	if (mkfifo(tuberia, 0666)<0) {
		perror("No puede crearse el FIFO");
		return(1);
	}
	//Lo abrimos para solo lectura
	if ((fd=open(tuberia, O_RDONLY ))<0) {
		perror("No puede abrirse el FIFO");
		return(1);
	}
	while (1) {
		if(read(fd, cadena, sizeof(cadena))<0){
			perror("No puede leerse el FIFO");
			return(1);
		}	
		printf("%s \n", cadena);
	}
	close(fd);
	unlink(tuberia);
	return(0);
}
